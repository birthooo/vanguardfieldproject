package com.shootingstar.vanguardfield.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.shootingstar.vanguardfield.R;
import com.shootingstar.vanguardfield.view.UnitCircleViewGroup;

public class MainFragment extends Fragment {

    View rootView;
    UnitCircleViewGroup unitV, unitRFrontLeft, unitRFrontRight, unitRBackLeft, unitRBackCenter, unitRBackRight,
            unitA1, unitA2, unitA3, unitA4;
    AlertDialog.Builder confirmResetDialogBuilder;

    private AdView mAdView;

    ImageView ivReset;

    public MainFragment() {
        super();
    }

    @SuppressWarnings("unused")
    public static MainFragment newInstance() {
        MainFragment fragment = new MainFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init(savedInstanceState);

        if (savedInstanceState != null)
            onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_main, container, false);

        initInstances(rootView, savedInstanceState);

        return rootView;
    }

    private void init(Bundle savedInstanceState) {
        // Init Fragment level's variable(s) here
    }

    @SuppressWarnings("UnusedParameters")
    private void initInstances(View rootView, Bundle savedInstanceState) {
        // Init 'View' instance(s) with rootView.findViewById here

        unitV = rootView.findViewById(R.id.v);
        setCircleBackgroundImage(unitV, R.drawable.vg_vg);

        unitRFrontLeft = rootView.findViewById(R.id.rFrontLeft);
        unitRFrontRight = rootView.findViewById(R.id.rFrontRight);
        unitRBackLeft = rootView.findViewById(R.id.rBackLeft);
        unitRBackCenter = rootView.findViewById(R.id.rBackCenter);
        unitRBackRight = rootView.findViewById(R.id.rBackRight);
        setCircleBackgroundImage(unitRFrontLeft, R.drawable.vg_rg);
        setCircleBackgroundImage(unitRFrontRight, R.drawable.vg_rg);
        setCircleBackgroundImage(unitRBackLeft, R.drawable.vg_rg);
        setCircleBackgroundImage(unitRBackCenter, R.drawable.vg_rg);
        setCircleBackgroundImage(unitRBackRight, R.drawable.vg_rg);

        unitA1 = rootView.findViewById(R.id.a1);
        unitA2 = rootView.findViewById(R.id.a2);
        unitA3 = rootView.findViewById(R.id.a3);
        unitA4 = rootView.findViewById(R.id.a4);
        setCircleBackgroundImage(unitA1, R.drawable.vg_imaginary_gift);
        setCircleBackgroundImage(unitA2, R.drawable.vg_imaginary_gift);
        setCircleBackgroundImage(unitA3, R.drawable.vg_imaginary_gift);
        setCircleBackgroundImage(unitA4, R.drawable.vg_imaginary_gift);

        mAdView = rootView.findViewById(R.id.adView);
        mAdView.loadAd(new AdRequest.Builder().build());

        ivReset = rootView.findViewById(R.id.ivReset);
        ivReset.setOnClickListener(onClickListener);

        confirmResetDialogSetting();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /*
     * Save Instance State Here
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
    }

    /*
     * Restore Instance State Here
     */
    @SuppressWarnings("UnusedParameters")
    private void onRestoreInstanceState(Bundle savedInstanceState) {
        // Restore Instance State here
    }

    /************
     * Listener
     ***********/

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ivReset :
                    confirmResetDialogBuilder.show();
                    break;
                default: break;
            }
        }
    };

    /***********
     * function
     ***********/

    private void setCircleBackgroundImage(UnitCircleViewGroup mUnit, int imgId) {
        ImageView mImageView = mUnit.findViewById(R.id.iv_background);
        mImageView.setImageResource(imgId);
    }

    private void resetField() {
        unitV.resetPower();
        unitRFrontLeft.resetPower();
        unitRFrontRight.resetPower();
        unitRBackLeft.resetPower();
        unitRBackCenter.resetPower();
        unitRBackRight.resetPower();
        unitA1.resetPower();
        unitA2.resetPower();
        unitA3.resetPower();
        unitA4.resetPower();
    }

    public void confirmResetDialogSetting() {
        confirmResetDialogBuilder = new AlertDialog.Builder(getContext());
        confirmResetDialogBuilder.setMessage("RESET ?");
        confirmResetDialogBuilder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                resetField();
            }
        });
        confirmResetDialogBuilder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
    }
}
