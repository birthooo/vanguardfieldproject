package com.shootingstar.vanguardfield.view;

import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import com.shootingstar.vanguardfield.R;

public class UnitCircleViewGroup extends FrameLayout {

    TextView tvPower;
    ImageButton ibUp, ibDown;

    Integer currentPower;

    final int PLUS_MODE = 1;
    final int MINUS_MODE = 2;
    final int TRIGGER_MODE = 3;
    final int CLEAR_MODE = 4;

    public UnitCircleViewGroup(@NonNull Context context) {
        super(context);
        initInflate();
    }

    public UnitCircleViewGroup(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initInflate();
    }

    public UnitCircleViewGroup(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initInflate();
    }

    @TargetApi(21)
    public UnitCircleViewGroup(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initInflate();
    }

    private void initInflate() {
        inflate(getContext(), R.layout.unit_circle, this);

        tvPower  = findViewById(R.id.tv_power);
        ibUp  = findViewById(R.id.ib_up);
        ibDown  = findViewById(R.id.ib_down);

        currentPower = Integer.parseInt(tvPower.getText().toString());

        ibUp.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                powerTextChange(PLUS_MODE);
            }
        });

        ibUp.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                powerTextChange(TRIGGER_MODE);
                // set TRUE if don't want onClick trigger
                return true;
            }
        });

        ibDown.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentPower > 0) {
                    powerTextChange(MINUS_MODE);
                }
            }
        });

        ibDown.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                powerTextChange(CLEAR_MODE);
                // set TRUE if don't want onClick trigger
                return true;
            }
        });

    }

    private void powerTextChange(int mode) {

        final int changedPower;
        if (mode == PLUS_MODE)
            changedPower = currentPower + 1000;
        else if (mode == MINUS_MODE)
            changedPower = currentPower - 1000;
        else if (mode == TRIGGER_MODE)
            changedPower = currentPower + 10000;
        else
            changedPower = 0;

        ValueAnimator animator = ValueAnimator.ofInt(currentPower, changedPower);
        animator.setDuration(100);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                tvPower.setText(animation.getAnimatedValue().toString());
                currentPower = changedPower;
            }
        });
        animator.start();
    }

    public void resetPower() {
        tvPower.setText("0");
    }
}
